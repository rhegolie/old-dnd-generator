<?php

$kobold_loot = array(
  array('d12' => '1','treasure' => rand(1,100).' Caltrop(s)','description' => 'One of the worst things to accidentally step on!'),
  array('d12' => '2','treasure' => 'Crate of Centipedes','description' => 'This wooden crate contains live centipedes.'),
  array('d12' => '3','treasure' => 'Bag of Mushrooms','description' => 'For those brave enough, surprisingly nutritious.'),
  array('d12' => '4','treasure' => 'Silver Nugget','description' => 'One pound nugget, worth about 5 GP.'),
  array('d12' => '5','treasure' => 'Spider Pot','description' => 'A clay pot filled with poisonous spiders. Eeeek~'),
  array('d12' => '6','treasure' => 'Golden Armband','description' => 'For small creatures, fits snuggly for stealth.'),
  array('d12' => '7','treasure' => rand(1,4).' Unpolished Gem(s)','description' => 'A variety of rough semi-precious gemstones.'),
  array('d12' => '8','treasure' => 'Throwing Net','description' => 'This net is for throwing at an enemy to restrain.'),
  array('d12' => '9','treasure' => 'Bearded Dragon','description' => 'A pet lizard, likes to bite at toes.'),
  array('d12' => '10','treasure' => 'Bear Trap','description' => 'Kobolds are always prepered against bears.'),
  array('d12' => '11','treasure' => rand(1,4).' Platinum Ring(s)','description' => 'For small fingers, very beautiful and shiny.'),
  array('d12' => '12','treasure' => 'Pot of Green Slime','description' => 'Acidic slime that devours flesh, organic material and metal (DMG 105).')
);

$goblin_loot = array(
    array('d12' => '1','treasure' => rand(1,4).' Decomposing Fish Head(s)','description' => 'So that\'s where that smell came from...'),
    array('d12' => '2','treasure' => 'Pocket of Sand','description' => 'Enemy adventurer\'s accuracy fell!'),
    array('d12' => '3','treasure' => 'Jar of Pickled Toes','description' => 'A common goblinoid delicacy.'),
    array('d12' => '4','treasure' => 'A Dagger','description' => 'Just a standard dagger.'),
    array('d12' => '5','treasure' => 'Pet Squirrel','description' => 'It\'s sufferring from a bad case of mange.'),
    array('d12' => '6','treasure' => 'Skull Bomb','description' => 'Throw it and run.'),
    array('d12' => '7','treasure' => 'Necklace of Teeth','description' => 'The newest fashion statement.'),
    array('d12' => '8','treasure' => 'Cracked Porcelain Doll','description' => 'In fine clothes, but missing it\'s eyes.'),
    array('d12' => '9','treasure' => 'Peg Leg','description' => '"I\'m gonna get that leg."'),
    array('d12' => '10','treasure' => 'A Coin Purse','description' => 'Jackpot! Contains '.rand(1,4).'GP, '.rand(2,12).'SP, '.rand(3,24).'CP.'),
    array('d12' => '11','treasure' => 'Glowing Pheasant Carcass','description' => 'There\'s a magical glowing gem in the gizzard.'),
    array('d12' => '12','treasure' => 'Wand of Burning Hands','description' => 'A burnt hand on a stick. Casts the spell Burning Hands once per day.')
);

$skeleton_loot = array(
    array('d12' => '1','treasure' => 'Bucket','description' => 'It\'s a bucket. There is nothing inside.'),
    array('d12' => '2','treasure' => rand(1,6).' Mushroom(s)','description' => 'The mushrooms are growing inside the skull.'),
    array('d12' => '3','treasure' => 'Order of Battle','description' => 'States, "Never give in! Never stop fighting!"'),
    array('d12' => '4','treasure' => 'Armor Scraps','description' => 'An assortment of leathers and broken chain mail.'),
    array('d12' => '5','treasure' => 'Scared Mouse','description' => 'Seems like it has been trapped in it\'s ribcage.'),
    array('d12' => '6','treasure' => 'Bone Dice','description' => 'Set of dice made out of bones.'),
    array('d12' => '7','treasure' => 'Patched Cape','description' => 'A cape made up of various different cloths.'),
    array('d12' => '8','treasure' => 'Silken Cord Amulet','description' => 'Crafted from iron to resemble a heart.'),
    array('d12' => '9','treasure' => 'Corroded Mail Circlet','description' => 'Provides your forehead with protection.'),
    array('d12' => '10','treasure' => 'Golden Ring','description' => 'This ring is set with a carnelian.'),
    array('d12' => '11','treasure' => 'Villainus Skull','description' => 'The skull glows a constant eerie green light.'),
    array('d12' => '12','treasure' => 'Skeleton Key','description' => 'Casts a Knock spell to unlock almost any door, gate and chest. One use per short rest.')
);

$bandit_loot = array(
    array('d12' => '1','treasure' => 'Red Bandana','description' => 'Don\'t have to worry about blood stains!'),
    array('d12' => '2','treasure' => 'Steel Lock','description' => 'There\'s no key.'),
    array('d12' => '3','treasure' => 'Playing Card Set','description' => 'Some of the corners are bent.'),
    array('d12' => '4','treasure' => 'Rusted Scimitar','description' => 'It\'s in a bad shape, -1 damage rolls.'),
    array('d12' => '5','treasure' => 'Fingerless Gloves','description' => 'Makes it easier to snag things.'),
    array('d12' => '6','treasure' => 'Six Sided Dice','description' => 'Weights much more than it should.'),
    array('d12' => '7','treasure' => 'Gold Earrings','description' => 'A set of very beautiful golden earrings.'),
    array('d12' => '8','treasure' => 'Double-Headed Coin','description' => 'The coin has two faces.'),
    array('d12' => '9','treasure' => 'Vial of Acid','description' => 'On a hit, the target takes '.rand(2,12).' acid damage.'),
    array('d12' => '10','treasure' => 'Wooden Box of Coin','description' => 'Contains '.rand(2,12).'GP, '.rand(3,24).'SP, '.rand(4,48).'CP.'),
    array('d12' => '11','treasure' => 'A Fine, Leather Gem Pouch','description' => '100 GP worth of shiny gemstones.'),
    array('d12' => '12','treasure' => 'Poison Bolt','description' => 'Poison vial stuck to a bolt. CON save or take an additional '.rand(1,4).' poison damage.')
);

$orc_loot = array(
    array('d12' => '1','treasure' => 'Pair of Ears','description' => 'A nice set of human ears tied together.'),
    array('d12' => '2','treasure' => 'White Gloves','description' => 'Made of 100% elven flesh.'),
    array('d12' => '3','treasure' => 'Pouch of Stones','description' => 'This pouch contains five lumpy stones.'),
    array('d12' => '4','treasure' => 'Cave Bear Paw','description' => 'The paw of a bear that lives in a cave.'),
    array('d12' => '5','treasure' => 'Three Paint Pots','description' => 'Three pigments: red ochre, gray ash, charcoal'),
    array('d12' => '6','treasure' => 'Bone Necklace','description' => 'Gnomish bones, said to bring good luck.'),
    array('d12' => '7','treasure' => 'Sack of Rations','description' => 'Contains '.rand(1,12).' worth of rations.'),
    array('d12' => '8','treasure' => 'Giant Bat Fang','description' => 'The tooth is stained red, probably from blood.'),
    array('d12' => '9','treasure' => 'Dire Wolf Hide','description' => 'Suprisingly soft and cozy.'),
    array('d12' => '10','treasure' => 'Treasure Chest','description' => 'Stolen coins worth '.rand(3,24).'GP, '.rand(3,30).'SP, '.rand(3,36).'CP.'),
    array('d12' => '11','treasure' => 'Bone Javelin','description' => 'Finely crafted, made purely from a large bone.'),
    array('d12' => '12','treasure' => 'Bear Hide Armor','description' => 'Grants the benefits of hide armor, with the aesthetic of a hood made from the bears head.')
);

$guard_loot = array(
    array('d12' => '1','treasure' => 'Happy Fun Rock','description' => 'A lumpy little rock with happy face painted on it.'),
    array('d12' => '2','treasure' => 'Love Letter','description' => 'The note is a proclamation of romantic interest.'),
    array('d12' => '3','treasure' => 'Fine Aged Cheddar','description' => 'Block of cheese, attracts mice.'),
    array('d12' => '4','treasure' => 'Ring of Keys','description' => 'Six iron keys on a key ring.'),
    array('d12' => '5','treasure' => 'Signal Whistle','description' => 'A small whistle to signal other guards.'),
    array('d12' => '6','treasure' => 'Golden Locket','description' => 'Inside the locket is a painting of two small children.'),
    array('d12' => '7','treasure' => 'Hardcover Book','description' => 'Titled "A Guide On How To Be The Best Guard".'),
    array('d12' => '8','treasure' => 'Official Tabard','description' => 'Sleeveless garment with official symbol and colors.'),
    array('d12' => '9','treasure' => 'Clue Scroll','description' => 'Written in thieves cant, it is a treasure hunt.'),
    array('d12' => '10','treasure' => 'Silver Flask','description' => 'Midway full of a strong and harsh brew of ale.'),
    array('d12' => '11','treasure' => 'Health Potion','description' => 'Red liquid, regain '.rand(4,10).' HP when consumed.'),
    array('d12' => '12','treasure' => 'Reinforced Lockbox','description' => 'Difficult to unlock, contains a bronze crown worth 250GP.')
);