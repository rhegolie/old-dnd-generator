<?php 
session_start();
include("nagl.php");
include("dnd_races.php");
include("dnd_class.php");
include("dnd_background.php");

if ($_SESSION['counter']==0){
    $_SESSION['range'] = $_POST['range'];
    $_SESSION['counter'] = $_SESSION['counter']+1;
}

$range = trim($_SESSION['range']);

#race
$roll=rand(1,100);
if($roll>$range){
    $which=rand(0,14);
    $race = $dnd_races[$which]['race1'];
    $subrace = $dnd_races[$which]['sub1'];
    $source_race = $dnd_races[$which]['source1'];
}else{
    $which=rand(0,76);
    $race = $dnd_races[$which]['race2'];
    $subrace = $dnd_races[$which]['sub2'];
    $source_race = $dnd_races[$which]['source2'];
}

#class
$roll=rand(1,100);
if($roll>$range){
    $which=rand(0,39);
    $class = $dnd_class[$which]['class1'];
    $subclass = $dnd_class[$which]['sub1'];
    $source_class = $dnd_class[$which]['source1'];
}else{
    $which=rand(0,76);
    $class = $dnd_class[$which]['class2'];
    $subclass = $dnd_class[$which]['sub2'];
    $source_class = $dnd_class[$which]['source2'];
}

#background
$roll=rand(1,100);
if($roll>$range){
    $which=rand(0,17);
    $background = $dnd_background[$which]['background1'];
    $source_background = $dnd_background[$which]['source1'];
}else{
    $which=rand(0,43);
    $background = $dnd_background[$which]['background2'];
    $source_background = $dnd_background[$which]['source2'];
}

#alignment
#$alignment=array('Lawful Good', 'Neutral Good', 'Chaotic Good', 'Lawful Neutral', 'True Neutral', 'Chaotic Neutral','Lawful Evil','Neutral Evil','Chaotic Evil');
#$alignment=$alignment[rand(0,8)];
$gne=array('Good','Neutral','Evil');
$lnc=array('Lawful','Neutral','Chaotic');
$gne=$gne[rand(0,2)]; $lnc=$lnc[rand(0,2)];
if($gne==$lnc){$alignment="True ".$gne;}else{$alignment=$lnc." ".$gne;}

echo "<br><br><center><table border><tr><td>race:</td><td>".$race."</td></tr><tr><td>subrace:</td><td>".$subrace."</td></tr>";
echo "<tr><td>class:</td><td>".$class."</td></tr><tr><td>subclass:</td><td>".$subclass."</td></tr>";
echo "<tr><td>background:</td><td>".$background."</td></tr>";
echo "<tr><td>alignment:</td><td>".$alignment."</td></tr></table></center><br>";

echo "<h5>Where to look for race description: ".$source_race."<br>";
echo "Where to look for class description: ".$source_class."<br>";
echo "Where to look for background description: ".$source_background."</h5>";
?>

<center><table><tr><td>
<form action="character.php" method="post"> 
       <input type="submit" value="Another">
</form></td><td>
<form action="index.php" method="get"> 
       <input type="submit" value="Return">
</form></td></tr>
</table></center>

<br>
<hr>
<h5>
Source: https://docs.google.com/spreadsheets/d/1fjnoCiYUCOcrMxffWNwppHfuUDlY1Ae0HQ4t-XCaiPA/edit#gid=0;<br>
D&D 5e Classes and Sub-classes by Pseudocide759;<br>
https://5etools.com/backgrounds.html#acolyte_phb
</h5>
</body>
</html>
<?php 
$conn->close();?>