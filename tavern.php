<?php
session_start();
include("nagl.php");
include("dnd_tavern.php");
include("dnd_class.php");

if ($_SESSION['counter']==0){
$_SESSION['counter'] = $_SESSION['counter']+1;
#print_r($_SESSION);
}#else{print_r($_SESSION);}

#name
$roll=rand(1,10);
switch ($roll) {
    case 1:
        $x = rand(0,199);
        $a = $dnd_tavern[$x]['adjective'];
        $y = rand(0,199);
        $b = $dnd_tavern[$y]['noun'];
        $name="Under the ".$a." ".$b."<br>";
        break;
    case 2:
        $x = rand(0,199);
        $a = $dnd_tavern[$x]['adjective'];
        $y = rand(0,199);
        $b = $dnd_tavern[$y]['noun'];
        $name="The ".$a." ".$b."<br>";
        break;
    case 3:
        $x = rand(0,199);
        $a = $dnd_tavern[$x]['adjective'];
        $y = rand(0,199);
        $b = $dnd_tavern[$y]['noun'];
        $z = rand(0,15);
        $c = $dnd_tavern[$z]['title'];
        $name="The ".$a." ".$b." ".$c."<br>";
        break;
    case 4:
        $x = rand(0,199);
        $a = $dnd_tavern[$x]['noun'];
        $y = rand(0,199);
        $b = $dnd_tavern[$y]['noun'];
        $name="The ".$a."'s ".$b."<br>";
        break;
    case 5:
        $x = rand(0,199);
        $a = $dnd_tavern[$x]['noun'];
        $y = rand(0,199);
        $b = $dnd_tavern[$y]['noun'];
        $z = rand(0,199);
        $c = $dnd_tavern[$z]['noun'];
        $name=$a.", ".$b." & ".$c."<br>";
        break;
    case 6:
        $x = rand(0,199);
        $a = $dnd_tavern[$x]['noun'];
        $y = rand(0,199);
        $b = $dnd_tavern[$y]['noun'];
        $name="The ".$a." & ".$b."<br>";
        break;
    case 7:
        $x = rand(0,199);
        $a = $dnd_tavern[$x]['noun'];
        $z = rand(0,15);
        $c = $dnd_tavern[$z]['title'];
        $name="The ".$a."'s ".$c."<br>";
        break;
    case 8:
        $x = rand(0,199);
        $a = $dnd_tavern[$x]['adjective'];
        $z = rand(0,15);
        $c = $dnd_tavern[$z]['title'];
        $name="The ".$a." ".$c."<br>";
        break;
    case 9:
        $x = rand(0,199);
        $a = $dnd_tavern[$x]['adjective'];
        $y = rand(0,199);
        $b = $dnd_tavern[$y]['noun'];
        $name=$a." ".$b."<br>";
        break;
    case 10:
        $x = rand(0,199);
        $a = $dnd_tavern[$x]['noun'];
        $y = rand(0,199);
        $b = $dnd_tavern[$y]['adjective'];
        $y = rand(0,199);
        $c = $dnd_tavern[$z]['noun'];
        $name="The ".$a."'s ".$b." ".$c."<br>";
        break;
}

#service
$x = rand(0,11);
$service = $dnd_tavern[$x]['services'];

#illegal
$x = rand(0,12);
$illegal = $dnd_tavern[$x]['illegal'];

#feature
$x = rand(0,19);
$feature = $dnd_tavern[$x]['feature'];

#location
$x = rand(27,36);
$location = $dnd_tavern[$x]['feature'];

#fame
$x = rand(19,48);
$fame = $dnd_tavern[$x]['illegal'];

#food
$x = rand(19,48);
$food = $dnd_tavern[$x]['services'];

#decor
$x = rand(19,48);
$decor = $dnd_tavern[$x]['title'];

#construct
if(rand(0,1)){$construct='1-story';}else{$construct='1-story';}
$x = rand(55,59);
$construct.=" building made of ".$dnd_tavern[$x]['feature'];

#owner
$roll=rand(1,10);
switch ($roll) {
    case 1:
        $x = rand(0,39);
        $a = $dnd_class[$x]['class1'];
        $name="Retired ".$a."<br>";
        break;
    case 2:
        $x = rand(0,39);
        $a = $dnd_class[$x]['class1'];
        $name="Retired ".$a."<br>";
        break;
}

$table="<br><br><br><center><table border bgcolor='#ffedaf'>";
$table.="<tr><td> Name of the tavern </td><td><b>".$name."</b></td></tr>";
$table.="<tr><td> Location </td><td><b>".$location."</b></td></tr>";
$table.="<tr><td> Known for </td><td><b>".$fame."</b></td></tr>";
$table.="<tr><td> Construction </td><td><b>".$construct."</b></td></tr>";
$table.="<tr><td> Features of note </td><td><b>".$decor."</b></td></tr>";
$table.="<tr><td> Unique feature </td><td><b>".$feature."</b></td></tr>";
$table.="<tr><td> Offered service </td><td><b>".$service."</b></td></tr>";
$table.="<tr><td> Main dish </td><td><b>".$food."</b></td></tr>";
$table.="<tr><td> Illegal side business </td><td><b>".$illegal."</b></td></tr>";
$table.="</table></center>";
echo $table;
?>


<center><table><tr><td>
<form action="tavern.php" method="post"> 
       <input type="submit" value="Another">
</form></td><td>
<form action="index.php" method="get"> 
       <input type="submit" value="Return">
</form></td></tr>
</table></center>

<?php 
#$conn->close();
?>
<br>
<hr>
<h5>
Source: Tavern roll table. from @AhoyhoyJet Roll a random tavern/pub encounter for Pathfinder/DnD<br>
http://savevsdragon.blogspot.com/2012/08/d30-sandbox-week-day-3-d30-tavern.html 
</h5>
</body>
</html>
